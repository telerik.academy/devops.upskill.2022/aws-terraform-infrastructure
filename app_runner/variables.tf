variable "region" {
 description = "AWS region"
 type = string
}

variable "ECR_REPOSITORY"{
    type = string
}
