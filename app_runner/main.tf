resource "aws_apprunner_service" "BookStoreApi" {
  service_name = "BookStoreApi"

  source_configuration {
    image_repository {
        image_configuration {
        port = "80"
      }

      image_identifier      = "${var.ECR_REPOSITORY}:latest"
      image_repository_type = "ECR"
    }

    authentication_configuration {
        access_role_arn = "arn:aws:iam::529320429701:role/service-role/AppRunnerECRAccessRole"
      }
      
    auto_deployments_enabled = true
  }

  instance_configuration {
          cpu = "1 vCPU"
          memory = "2 GB"
        }

  health_check_configuration {
          healthy_threshold = "1"
          interval = "10"
          timeout = "5"
          unhealthy_threshold = "5"
        }

  tags = {
    Name = "playground-apprunner-service"
  }
}