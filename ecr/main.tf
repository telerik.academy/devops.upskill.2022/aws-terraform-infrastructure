resource "aws_ecr_repository" "ecr_repo" {
 name                 = var.ecr_repository_name
 image_tag_mutability = "MUTABLE"

 image_scanning_configuration {
   scan_on_push = true
  }

  encryption_configuration{
    encryption_type = "KMS"
 }
}

output "ecr_repository_url" {
    value = aws_ecr_repository.ecr_repo.repository_url
}
