variable "region" {
 description = "AWS region"
 type = string
}

variable "ecr_repository_name" {
    description = "Repository name"
    type = string
}
