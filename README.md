# aws-terraform-infrastructure

# Summary

This repository was created for educational purposes! 
It simulates IaC repository provisioning several resources to AWS (ECR & AppRunner).

#

- ECR terraform configuration:
    - Immage tags are set to be mutable (This is on purpose as AppRunner for BookStoreApi is set to Automatic deployments when latest tag in ecr is changed)
- AppRunner terraform configuration:
    - image_identifier is set to the latest tag in the ECR repository
    - auto_deployments_enabled is set to true so we allways have latest stable version deployed
- Implements pipline with several stages in the following order:
    - security-vulnerability-check stage: performs vulnerability chech with Checkovm creates HTML report & submits the results to www.bridgecrew.cloud. This job will be triggerd on every push to the remote and will run for both ECR & AppRunner resources simultaneously.
    - provision-resources stage: Has two manual jobs for provisioning ECR & AppRunner, so they cam be run on demand. The provisioning jobs create artifacts for the last plan created by terraform plan and terraform.tfstate file.
    - destroy-resources stage: Respectively this stage has two manual jobs for destroying the resources that were previously created.
    - P.S. the ugly code duplication will be fixed in future commits.
